// NextJS Imports
import Head from 'next/head';

// Component Imports
import Date from '../../components/date';
import Layout from '../../components/layout';

// Lib Imports
import { EMBEDS } from '@/lib/embeds';
import { getAllPostIds, getPostData } from '../../lib/posts';

// Styling Imports
import utilStyles from '../../styles/utils.module.css';

export async function getStaticPaths() {
  const paths = getAllPostIds();
  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  // Add the "await" keyword like this:
  const postData = await getPostData(params.id);

  return {
    props: {
      postData,
    },
  };
}


export default function Post({ postData }) {
  return (
    <Layout>
      {/* Page Head */}
      <Head>
        <title>{postData.title}</title>
      </Head>
      {/* Page Content */}
      <article>
        <h1 className={utilStyles.headingXl}>{postData.title}</h1>
        <div className={utilStyles.lightText}>
          <Date dateString={postData.date} />
        </div>
        <br/>
        {EMBEDS[postData.id] && (
          <iframe width="560" height="315" src={EMBEDS[postData.id]} title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowFullScreen></iframe>
        )}
        <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} />
      </article>
    </Layout>
  );
}

