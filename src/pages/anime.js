// NextJS Imports
import React from 'react'
import Head from 'next/head';
// Componen Imports
import Layout from '@/components/layout'

// Styles
import utilStyles from '../styles/utils.module.css';

// Global Variables
const pageName = "Anime"

const Anime = () => {
  return (
    <div>
      <Layout>
        <Head>
          <title>{pageName}</title>
        </Head>
        <section className={utilStyles.headingMd}>
          <h2>{pageName}</h2>
          <p>A storm wakes on the horizon...</p>
        </section>
      </Layout>
    </div>
  )
}

export default Anime