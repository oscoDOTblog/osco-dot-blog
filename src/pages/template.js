// NextJS Imports
import React from 'react'
import Head from 'next/head';

// Componen Imports
import Layout from '@/components/layout'

// Styles
import utilStyles from '../styles/utils.module.css';

// Global Variables
const pageName = "Template"

const Template = () => {
  return (
    <div>
      <Layout>
        <Head>
          <title>{pageName}</title>
        </Head>
        <section className={utilStyles.headingMd}>
          <h2>{pageName}</h2>
          <p>TODO: UPDATE HERE</p>
        </section>
      </Layout>
    </div>
  )
}

export default Template