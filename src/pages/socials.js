// NextJS Imports
import React from 'react'
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';

// Componen Imports
import Layout from '@/components/layout'

// Library Imports
import { SOCIALS } from '@/lib/config';

// Styling Imports
import utilStyles from '../styles/utils.module.css';

// Global Variables
const pageName = "Socials"

const Socials = () => {
  return (
    <div>
      <Layout>
        <Head>
          <title>{pageName}</title>
        </Head>
        <section className={utilStyles.headingMd}>
          <h2>{pageName}</h2>
          {SOCIALS.map(({name, desc, link}) => (
            <p key={name}>
              <Link legacyBehavior href={link}>
                <a target="_blank" rel="noopener noreferrer">
                  {name}
                </a> 
              </Link> - {desc}
            </p>
          ))}
        </section>
      </Layout>
    </div>
  )
}

export default Socials