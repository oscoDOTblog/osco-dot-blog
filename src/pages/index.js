// NextJS Imports
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";

// Component Imports
import Date from "../components/date";
import Layout, { siteTitle } from "../components/layout";
import NewsletterButton from "@/components/NewsletterButton";
import FooterSocial from "@/components/FooterSocial";

// Library Imports
import {
  COLLABORATIONS,
  MAIN_PROJECT,
  SITE_NAME,
  SITE_TITLE,
} from "../lib/config";
import { getSortedPostsData } from "../lib/posts";

// Styling Imports
import copyStyles from "../styles/copy.module.css";
import utilStyles from "../styles/utils.module.css";

export async function getStaticProps() {
  const allPostsData = getSortedPostsData();
  return {
    props: {
      allPostsData,
    },
  };
}

export default function Home({ allPostsData }) {
  const ExternalLink = ({ name, url }) => {
    return (
      <Link legacyBehavior href={url}>
        <a target="_blank" rel="noopener noreferrer">
          {name}
        </a>
      </Link>
    );
  };

  return (
    <div>
      <Layout home>
        <Head>
          <title>{SITE_NAME}</title>
          {/* Plausible Analytics Tracking */}
          <script
            defer
            data-domain="osco.blog"
            src="https://plausible.atemosta.com/js/script.js"
          ></script>
        </Head>
        {/* Intro */}
        <section className={utilStyles.headingMd}>
          <center>
            <h3>
              <i>{SITE_TITLE}</i>
            </h3>
          </center>
        </section>

        {/* Projects */}
        {/* <section className={utilStyles.headingMd}>
          <h2>Projects - Stuff I'm Working On</h2>
          {PROJECTS.map(({name, desc, link}) => (
            <p key={name}>
              <Link legacyBehavior href={link}>
                <a target="_blank" rel="noopener noreferrer">
                  {name}
                </a>
              </Link> - {desc}
            </p>
          ))}
        </section> */}

        {/* Intro */}
        <section className={utilStyles.headingMd}>
          <center>
            <Link legacyBehavior href={MAIN_PROJECT["link"]}>
              <a target="_blank" rel="noopener noreferrer">
                <Image
                  priority
                  src={MAIN_PROJECT["img"]}
                  className={utilStyles.borderCircle}
                  height={200}
                  width={200}
                  alt=""
                />
              </a>
            </Link>
          </center>

          <p>
            I'm currently working on{" "}
            <b>
              <ExternalLink
                name={MAIN_PROJECT["name"]}
                url={MAIN_PROJECT["link"]}
              />
            </b>
            , an app to teach you any dance at your own pace with mixed reality
            and AI!
          </p>

          <p>
            You can also visit my{" "}
            <Link href="/socials">
              <b>socials</b>
            </Link>{" "}
            to see what I'm up to, but I'm most active on my{" "}
            <Link href="https://x.com/oscoDOTblog">
              <b>Twitter (X)</b>
            </Link>{" "}
            account
          </p>
        </section>

        {/* Collaborations */}
        <section className={utilStyles.headingMd}>
          {/* Stuff I'm Working On */}
          <h4>Other Stuff I'm Working On...</h4>
          {COLLABORATIONS.map(({ name, desc, link, img }) => (
            <div key={name}>
              <p>
                <Link legacyBehavior href={link}>
                  <a target="_blank" rel="noopener noreferrer">
                    <b>{name}</b>
                  </a>
                </Link>{" "}
                - {desc}
              </p>
            </div>
          ))}
        </section>
        <br />

        {/* Blog Posts */}
        <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
          <h2 className={utilStyles.headingLg}>Latest Blog Posts</h2>
          <ul className={utilStyles.list}>
            {allPostsData.map(({ id, date, title }) => (
              <li className={utilStyles.listItem} key={id}>
                <Link href={`/posts/${id}`}>{title}</Link>
                <br />
                <small className={utilStyles.lightText}>
                  <Date dateString={date} />
                </small>
              </li>
            ))}
          </ul>
        </section>

        <h4>Wanna stay in touch?</h4>
        <NewsletterButton />
      </Layout>
    </div>
  );
}
