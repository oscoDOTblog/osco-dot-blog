import React, { useState } from 'react';
import buttonStyles from '../styles/copy.module.css';
import styles from '../styles/fadeUp.module.css';

const CopyToClipboard = ({ text }) => {
  const [copied, setCopied] = useState(false);

  const copyToClipboard = () => {
    const textField = document.createElement('textarea');
    textField.innerText = text;
    document.body.appendChild(textField);
    textField.select();
    document.execCommand('copy');
    textField.remove();
  };

  return (
  //   <button className={styles.button} onClick={copyToClipboard}>
  //     Copy to Clipboard
  //   </button>
  // );
    <>
      <button
        className={`${buttonStyles.button} ${copied ? styles.fade : ''}`}
        onClick={copyToClipboard}
      >
        {copied ? 'Copied!' : 'Copy to Clipboard'}
      </button>
    </>
  );
};

export default CopyToClipboard;

// const CopyToClipboard = ({ text }) => {
//   const copyToClipboard = () => {
//     const textField = document.createElement('textarea');
//     textField.innerText = text;
//     document.body.appendChild(textField);
//     textField.select();
//     document.execCommand('copy');
//     textField.remove();
//   };

//   return (
//     <button
//       className="button"
//       style={{
//         display: 'inline-block',
//         fontWeight: 500,
//         fontSize: '16px',
//         textAlign: 'center',
//         padding: '10px 20px',
//         backgroundColor: '#2196f3',
//         color: '#ffffff',
//         border: 'none',
//         borderRadius: '8px',
//         cursor: 'pointer',
//         transition: 'background-color 0.3s ease',
//         marginLeft: '10px'
//       }}
//       onClick={copyToClipboard}
//     >
//       Copy to Clipboard
//     </button>
//   );
// };

// export default CopyToClipboard;

// import React from 'react';

// const CopyToClipboard = ({ text }) => {
//   const copyToClipboard = () => {
//     const textField = document.createElement('textarea');
//     textField.innerText = text;
//     document.body.appendChild(textField);
//     textField.select();
//     document.execCommand('copy');
//     textField.remove();
//   };

//   return (
//     <div style={{ display: 'flex', alignItems: 'center' }}>
//       <p style={{ marginRight: '10px' }}>{text}</p>
//       <button
//         className="button"
//         style={{
//           fontWeight: 500,
//           fontSize: '16px',
//           padding: '10px 20px',
//           backgroundColor: '#2196f3',
//           color: '#ffffff',
//           border: 'none',
//           borderRadius: '8px',
//           cursor: 'pointer',
//           transition: 'background-color 0.3s ease',
//         }}
//         onClick={copyToClipboard}
//       >
//         Copy to Clipboard
//       </button>
//     </div>
//   );
// };

// export default CopyToClipboard;
