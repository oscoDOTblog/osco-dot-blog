'use client'

import { Facebook, Twitter, Instagram, Linkedin } from 'lucide-react'

export default function SocialFooter() {
  const socialLinks = [
    { name: 'Facebook', icon: Facebook, href: 'https://facebook.com' },
    { name: 'Twitter', icon: Twitter, href: 'https://twitter.com' },
    { name: 'Instagram', icon: Instagram, href: 'https://instagram.com' },
    { name: 'LinkedIn', icon: Linkedin, href: 'https://linkedin.com' },
  ]

  return (
    <footer className="fixed bottom-0 left-0 w-full bg-gray-800 text-white py-4 shadow-lg">
      <div className="container mx-auto px-4">
        <div className="flex justify-center items-center space-x-6">
          {socialLinks.map((link) => (
            <a
              key={link.name}
              href={link.href}
              target="_blank"
              rel="noopener noreferrer"
              className="hover:text-gray-300 transition-colors duration-200"
              aria-label={`Visit our ${link.name} page`}
            >
              <link.icon className="w-6 h-6" />
            </a>
          ))}
        </div>
      </div>
    </footer>
  )
}