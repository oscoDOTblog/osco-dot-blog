import Link from 'next/link';
import React from 'react'
import buttonStyles from '../styles/Button.module.css';

const NewsletterButton = () => {
  return (
    <div>
      <Link href="https://news.osco.blog" passHref>
        <button className={buttonStyles.button}>
          Sign Up for My Newletter Here!!!
        </button>
      </Link>
    </div>
  )
}

export default NewsletterButton