const COLLABORATIONS = [
  {
    "name": "Osco's Stories",
    "desc": "A blog updated with the fictional stories I'm working on",
    "img": "/banners/stories-banner.jpg",
    "link": "https://stories.osco.blog"
  },
  {
    "name": "SAO Menu",
    "desc": "A fun demo of the iconic Sword Art Online menu in VR, created with Three.js",
    "img": "/banners/sao-menu.png",
    "link": "https://sao.atos.to/"
  },
  {
    "name": "Clown Math University",
    "desc": "An upcoming visual novel where you take the role of a new professor at a unusual university teaching cryptids how to integrate into human society",
    "img": "/banners/cmu-banner.jpg",
    "link": "https://bsky.app/profile/cmu-vn.bsky.social"
  },
  // {
  //   "name": "BackdropMP",
  //   "desc": "Three-ish recovering anime addicts give unsolicited advice on men's problems (MP) in the backdrop of their minds",
  //   "img": "/banners/mp-banner.jpg",
  //   "link": "https://www.tiktok.com/@backdropmp"
  // },
  // {
  //   "name": "Unspoken by Team Signcraft",
  //   "desc": "Learn American Sign Language (ASL) in mixed reality",
  //   "link": "https://github.com/RingoKam/unspoken"
  // },
]

const MAIN_PROJECT = {
  "name": "Sway XR",
  "desc": "An app to teach you any dance at your own pace with mixed reality and AI",
  "img": "/images/sway.jpg",
  "link": "https://sway.quest"
}

const SITE_NAME = "Heyo, I'm Osco!"
const SITE_TITLE = "I quit my corporate job to explore AI, VR, and Storytelling!"
const SOCIALS = [
  {
    "name": "Email",
    "desc": "osco+ohaiyo@osco.blog",
    "link": "https://tutanota.com/"
  },
  {
    "name": "Bluesky",
    "desc": "osco",
    "link": "https://bsky.app/profile/osco.bsky.social"
  },
  {   
    "name": "Instagram", 
    "desc": "oscoDOTblog", 
    "link": "https://www.instagram.com/oscodotblog/"
  },
  {   
    "name": "LunkedIn", 
    "desc": "oscoDOTblog", 
    "link": "https://www.linkedin.com/in/oscodotblog/"
  },
  {   
    "name": "Mastodon", 
    "desc": "@osco@mastodon.social", 
    "link": "https://mastodon.social/@oscoDOTblog"
  },
  // {   
  //   "name": "Odysee", 
  //   "desc": "@Osco", 
  //   "link": "https://odysee.com/@Osco:b"
  // },
  {   
    "name": "Threads", 
    "desc": "@oscoDOTblog", 
    "link": "https://www.threads.net/@oscoDOTblog"
  },
  {   
    "name": "Twitter", 
    "desc": "oscoDOTblog", 
    "link": "https://twitter.com/oscoDOTblog"
  },
  {   
    "name": "YouTube", 
    "desc": "@oscoDOTblog", 
    "link": "https://www.youtube.com/@oscoDOTblog"
  },
]
const TGC_LINK = "https://tgc.atos.to"

export {
  COLLABORATIONS,
  MAIN_PROJECT,
  SITE_NAME,
  SITE_TITLE,
  SOCIALS,
  TGC_LINK
}
