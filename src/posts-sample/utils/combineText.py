import json

with open('input/oscos-blog-ep-2.json', encoding='latin-1') as file:
    json_data = json.load(file)
    text_list = [item['text'] for item in json_data]
    combined_text = ' '.join(text_list)

    with open('output.txt', 'w') as output_file:
        output_file.write(combined_text)

print('File written successfully!')
